import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

def removeBlackDots(image):
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

    x, y = gray.shape
    for i in range(x):
        for j in range(y):
            if (gray[i][j] < 10):
                image[i][j]=255

    return image

def fillHoles(image):

    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    ret, binary = cv.threshold(gray,254,255,cv.THRESH_BINARY_INV)
    h, w = binary.shape[:2]
    imgFilled = binary.copy()
    mask = np.zeros((h+2, w+2), np.uint8)
    cv.floodFill(imgFilled, mask, (0,0), 255);
    result = cv.bitwise_not(imgFilled)
    teste = binary | result

    return teste

def getConvexHull(image, source):
    #lembrar  de usar a área para remover os quadrados


    hsv = cv.cvtColor(source.copy(), cv.COLOR_BGR2HSV)
    lower_blue = np.array([110,50,50])
    upper_blue = np.array([130,255,255])

    # Threshold the HSV image to get only blue colors
    mask = cv.inRange(source, lower_blue, upper_blue)

    x, y = mask.shape

    print(mask)

    gray = image.copy()

    contours, hierarchy = cv.findContours(gray, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    hull = []

    for i in range(len(contours)):
        hull.append(cv.convexHull(contours[i], False))

    drawing = np.zeros((gray.shape[0], gray.shape[1], 3), np.uint8)

    # draw contours and hull points
    for i in range(len(contours)):
        color = (255, 0, 0)
        cv.drawContours(image, hull, i, color, 1, 8)

    return image


def hitOrMiss(image):
    kernel = np.array((
        [0, 0, 0],
        [0, 1, 0],
        [0, 0, 0]), dtype="int")
    output_image = cv.morphologyEx(image.copy, cv.MORPH_HITMISS, kernel)

    return output_image

def showResult(input, output):
    plt.subplot(121),plt.imshow(input, cmap = 'gray')
    plt.title('Input Image'), plt.xticks([]), plt.yticks([])
    plt.subplot(122),plt.imshow(output, cmap = 'gray')
    plt.title('output Image'), plt.xticks([]), plt.yticks([])
    plt.show()
