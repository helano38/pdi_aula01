from collections import Counter
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


def get_mean( size, soma):
    return soma/size


def get_variance (array):
    k=0
    mean = get_mean(len(array), sum(array))
    for i in array:
        k = k + (i- mean)*(i -mean)
    return k/(len(array)-1)

def get_trend(array):
    occurance_count = Counter(array)
    return occurance_count.most_common(1)[0][0]

def get_covariance(image1, image2, mean1, mean2 ):
    k = 0.0
    size = len(image1)
    for i in range(size):
        k = k + (image1[i] - mean1)*(image2[i] - mean2)
    return k/(len(image1)-1)

def get_correlation(image1, image2):
    a1 = get_array_from_image(image1)
    a2 = get_array_from_image(image2)
    return np.correlate(a1, a2, mode='full')

def get_array_from_image(image):
    array = []
    rows, cols = image.shape
    k=0
    for i in range(rows):
            for j in range(cols):
                array.append(int(image[i,j]))
    return array



def plotDataAndCov(data):

    sns.distplot(data[:,0], color="#53BB04")
    sns.distplot(data[:,1], color="#0A98BE")
    plt.show()
    plt.close()
