import cv2
import util
import numpy
import pandas as pd

image1 = cv2.imread('imagens/WashingtonDC_01.TIF',cv2.IMREAD_GRAYSCALE)
image2 = cv2.imread('imagens/WashingtonDC_02.TIF',cv2.IMREAD_GRAYSCALE)
array_image1 = util.get_array_from_image(image1)
data = pd.Series(array_image1)
data_frame = pd.DataFrame(data)

array_image2 = util.get_array_from_image(image2)

sum_image1 = sum(array_image1)
mean_image1 = util.get_mean(image1.size, sum(array_image1))
variance_image1 = data_frame.var(0)
trend_image1 = util.get_trend(array_image1)

sum_image2 = sum(array_image2)
mean_image2 = util.get_mean(image2.size, sum(array_image2))
variance_image2 = data_frame.var(0)
trend_image1 = util.get_trend(array_image1)

covariance =  util.get_covariance(array_image1, array_image2, mean_image1, mean_image2)

print ("****************************")
print ("image size : ", image1.size)
print ("image rows*cols : ", image1.shape)
print ("image media : ", mean_image1)
print ("image variance : ", variance_image1)
print ("image trend : ", trend_image1)
print ("image covariance between image 1 and 2 : ", covariance )
print ("****************************")
#util.plotDataAndCov(covariance)
#print ("correlation between image 1 and image 2 : ", util.get_correlation(image1, image2))


#print ("pandas data ", data)
#print ("pandas data frame", data_frame)
#print ("contar ocorrência de numeros ", data_frame[0].value_counts() )



#cv2.imshow('imagem 1',)
#cv2.waitKey(0)
#cv2.destroyAllWindows()
